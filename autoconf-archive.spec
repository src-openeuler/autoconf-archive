Name:            autoconf-archive
Version:         2024.10.16
Release:         1
Summary:         Autoconf Macro Archive
License:         GPL-3.0 AND GPL-2.0-or-later
URL:             https://www.gnu.org/software/autoconf-archive/
Source0:         https://ftp.gnu.org/gnu/autoconf-archive/autoconf-archive-%{version}.tar.xz
BuildArch:       noarch
Requires:        autoconf
Enhances:        autoconf

%description
Autoconf Macro Archive aims to provide a central repository of useful
and tested Autoconf macros for software developers around the world.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure
%make_build

%install
%make_install
rm -frv %{buildroot}%{_infodir}/dir
rm -frv %{buildroot}%{_datadir}/doc

%files
%license COPYING*
%doc AUTHORS NEWS README TODO
%{_datadir}/aclocal/*.m4
%{_infodir}/autoconf-archive.info*

%changelog
* Sat Oct 26 2024 Funda Wang <fundawang@yeah.net> - 2024.10.16-1
- update to 2024.10.16
- Remove useless scripts due to file triggers
- Update licenses declaration

* Tue Feb 21 2023 wangkai <wangkai385@h-partners.com> - 2023.02.20-1
- Upgrade to version 2023.02.20

* Wed Jun 15 2022 SimpleUpdate Robot <tc@openeuler.org> - 2022.02.11-1
- Upgrade to version 2022.02.11

* Mon Jan 4 2021 Ge Wang <wangge20@huawei.com> - 2.18.03.13-4
- Modify license infomation

* Thu Nov 28 2019 fengbing <fengbing7@huawei.com> - 2018.03.13-3
- Package init

